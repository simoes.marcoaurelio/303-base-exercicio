FROM openjdk:8-alpine
WORKDIR /app
COPY target/Api-Investimentos-*.jar api-investimentos.jar 
CMD ["java", "-jar", "/app/api-investimentos.jar"]
